# Vue JS Boilerplate project

##### Important dependencies included
 * axios (http requests)
 * vue2-dropzone (image upload)
 * chartist (chars & graphs)
 * vee-validate (validations)
 * sweetalert2 (info, warning & error dialogs)
 * sass (via sass-loader)
 * bootstrap
 

##### Setup

* Clone repository
* Instal npm dependecies ```npm install```
* Run nmp script ```npm run dev```

##### File structure
![FileStructure](https://image.prntscr.com/image/c-oniMHTRcCdn8dOJJ-sGA.png)

##### TODO:
* Add service worker
* Add pwa support to template
* SEO server files (.httacces / robot permissions etc)
* Dependency injection logs
* Tree shaking (every lib that support modules should be exported as modules inside prod build , and only parts that are used should be imported)
* Add gzip mapping for prod build
* Write security check for all dependecies (log warrning messages on dev build output)
* Unify webpack configuration , remove testing utils
* Add yarn and lock configurations