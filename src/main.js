// libs import
import Vue from 'vue';
import VueRouter from 'vue-router';
import vClickOutside from 'v-click-outside';
import Moment from 'vue-moment';
import Chartist from 'chartist';
import VeeValidate from 'vee-validate';
import VueInstant from 'vue-instant';

// plugins import
import SessionService from "./services/session";
import SideBar from './components/Shared/SidebarPlugin';
import WebsocketConfiguration from './websocketConfiguration'
import SharedComponents from './sharedComponents';
import App from './App';

// routes
import routes from './routes/routes';

// packages import
import 'bootstrap/dist/css/bootstrap.css';
import 'sweetalert2/dist/sweetalert2.css';
import 'vue-instant/dist/vue-instant.css';
import 'es6-promise/auto';
import 'vue2-loading-bar';
import 'w3-css'

// styles inject
import './assets/sass/app.scss';

// plugin setup
Vue.use(VueRouter);
Vue.use(vClickOutside);
Vue.use(SideBar);
Vue.use(VeeValidate);
Vue.use(Moment);
Vue.use(VueInstant);
Vue.use(SharedComponents);

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    linkActiveClass: 'active'
});

export const EventBus = new Vue();

Vue.prototype.$eventBus = EventBus;

SessionService.init(router);
Vue.prototype.$sessionService = SessionService;

EventBus.$on('print-something', function () {
    console.log('something');
});

// global library setup
Object.defineProperty(Vue.prototype, '$Chartist', {
    get() {
        return this.$root.Chartist
    }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App),
    router,
    mounted() {
        //new WebsocketConfiguration(this, "http://localhost:8080/pushnotifications");
    },
    stompClient:{
        monitorIntervalTime: 100,
        stompReconnect: true
    }
})
