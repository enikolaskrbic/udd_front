import DropDown from './components/Shared/DropDown.vue';

/**
 * Register global components here and use them as a plugin in your main Vue instance
 */

const SharedComponents = {
    install (Vue) {
        Vue.component('drop-down', DropDown);
    }
};

export default SharedComponents
