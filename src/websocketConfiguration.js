let self,
    listOfSubscribeResults = [];

export default WebsocketConfiguration

function WebsocketConfiguration(vueInstance, websocketUrl) {
    self = vueInstance;
    self.connectWS(websocketUrl, {}, wsSuccess, wsError);

    self.$eventBus.$on('unsubscribeAll', function () {
        listOfSubscribeResults.forEach(function(subscribeResult) {
            subscribeResult.unsubscribe();
        });
        listOfSubscribeResults = [];
    });
}

function wsSuccess() {
    console.log("success")
    listOfSubscribeResults.push(self.$stompClient.subscribe('/user/test', subCallback));
}

function subCallback() {
    console.log("success");
    self.$eventBus.$emit('print-something');
}

function wsError() {
    console.log("error")
}


