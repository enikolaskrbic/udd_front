import MainView from '../components/Layout/MainView.vue'
import Login from '../components/Account/Login.vue'
import NotFound from '../components/Shared/NotFoundPage.vue'
import Home from '../components/Home/Home.vue';
import RegisterGenericFields from '../components/Generic/RegistrationForm.vue';
import AcceptMail from '../components/Account/AcceptMail.vue';
import Auction from '../components/Generic/AuctionForm.vue';
import RegistrationForm from "../components/Generic/RegistrationForm";

const routes = [
    {
        path: '/login',
        component: Login,
        name: 'login'
    },
    {
        path: '/',
        component: MainView,
        redirect: '/register',
        children: [
            {
                path: '/home',
                component: Home,
                name: 'home'

            },
            {
                path: '/register',
                component: RegisterGenericFields,
                name: 'register'

            },
            {
                path: '/submit-mail',
                component: AcceptMail,
                name: 'verification-mail'

            },
            {
                path: '/auction',
                component: Auction,
                name: 'auction'

            }
        ]
    },
    {
        path:
        '*',
        component: NotFound
    }
];

export default routes
