import TopNavbar from './TopNavbar.vue';
import DashboardContent from './Content.vue';

export default {

    methods: {
        toggleSidebar () {
            if (this.$sidebar.showSidebar) {
                this.$sidebar.displaySidebar(false)
            }
        }
    },

    components: {
        TopNavbar,
        DashboardContent
    }
}
