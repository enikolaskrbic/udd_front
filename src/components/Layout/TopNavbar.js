export default {
    computed: {
        routeName () {
            const {name} = this.$route
            return this.capitalizeFirstLetter(name)
        }
    },

    data () {
        return {
            activeNotifications: false,
            changePasswordModalVisible: false,
            korisnik : null
        }
    },

    methods: {
        capitalizeFirstLetter (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        },
        signOut () {
            this.$router.push('/login');
            localStorage.removeItem(localStorage.getItem('username'));
            localStorage.removeItem('username');
        },
        register () {
            this.$router.push('/register');

        }
    },
    beforeMount() {
        this.korisnik = localStorage.getItem('username');
    }
}
