import Sidebar from './SideBar.vue'

const NotLoggedUser = {
    showSidebar: false,
    sidebarLinks: [
        {
            name: 'login',
            icon: 'ti-panel',
            path: '/login'
        },
        {
            name: 'register',
            icon: 'ti-panel',
            path: '/register'
        }
    ],
    displaySidebar (value) {
        this.showSidebar = value
    }
}

const LoggedUser ={
    showSidebar: false,
    sidebarLinks: [
        {
            name: 'tasks',
            icon: 'ti-panel',
            path: '/tasks'
        },
        {
            name: 'auction',
            icon: 'ti-panel',
            path: '/auction'
        },
        {
            name: 'logout',
            icon: 'ti-panel',
            path: '/login'
        }
    ],
    displaySidebar (value) {
        this.showSidebar = value
    }
}

const SidebarPlugin = {

    install (Vue) {
        Vue.mixin({
            data () {
                return {
                    loggedUser: LoggedUser,
                    notLoggedUser: NotLoggedUser
                }
            }
        })

        Object.defineProperty(Vue.prototype, '$sidebar', {
            get () {
                if(localStorage.getItem('username')===null){
                    return this.$root.notLoggedUser
                }else{
                    return this.$root.loggedUser
                }
                
            }
        })
        Vue.component('side-bar', Sidebar)
    }
}

export default SidebarPlugin
