import restApi from '../../http-common'
import swal from 'sweetalert2'
import VueRouter from 'vue-router'
import api from '../../services/networkManager'

const router = new VueRouter();
const apiService = new api();

export default {
    name: 'login',
    data() {
        return {
            model: {
                email: '',
                password: ''
            }
        }
    },
    methods: {
        keymonitor(event) {
            if(event.key === 'Enter') {
                this.login();
            }
        },

        login() {
            apiService.signIn(this.model)
                .then((response) => {
                    if (response.status === 200 ) {
                        localStorage.setItem('username', response.data.korisnickoIme);
                        this.$router.push('/auction');
                    }
                })
                .catch((error) => {
                    swal('Error', 'Credentials invalid!', 'error');

                });
        }
    },
    components: {}
}
