import restApi from '../../http-common'
import swal from 'sweetalert2'
import VueRouter from 'vue-router'
import Api from '../../services/networkManager'

const router = new VueRouter();
let api = new Api();
export default {
    name: 'verification-mail',
    data() {
        return {
            userId: "",
            activeTasks: [
                {
                    id: "",
                    name : ""
                }
            ],
            model: []
        }
    },
    methods: {
        getUserId(){
            this.userId = window.location.href.split("=")[1];
            this.getActiveTask();
        },

        getActiveTask(){
            api.getActiveTask(this.userId)
                .then((response) => {
                    this.activeTasks = response.data;
                }).catch((error) => {

            });
        },

        submit() {
            api.submitForm(this.activeTasks[0].id,this.model)
                .then((response) => {
                   swal('Success',"Success verification!",'success');
                }).catch((error) => {

            });
        }
    },
    components: {},
    beforeMount() {
        this.getUserId();


    }
}
