import restApi from '../../http-common'
import swal from 'sweetalert2'
import VueRouter from 'vue-router'
import Api from '../../services/networkManager'

const router = new VueRouter();
const api = new Api();

export default {
    name: 'aukcija',
    data() {
        return {
            tempModel: [
                {
                    type: null,
                    name: null,
                    label: null,
                    value: null,
                    required: true,
                    writable: true,
                    readable: true,
                    values: [
                        {
                            name: "",
                            value: ""
                        }
                    ]
                }
            ],
            userId : "",
            activeTasks : [
                {
                    id: "",
                    name : ""
                }

            ],
            selectedCategory : "",
            addedCategory:"",
            listOfCategory : [
                {
                    id: "",
                    ime: "",
                    activitiId: "",
                    opis: "",
                    korisnickoIme: ""
                }
            ],
            loggedUser : "",
            selectedTask: {
                id: "",
                name : ""
            }
        }
    },
    methods: {
        startAuction(user){
            localStorage.setItem(user,"1");
            api.startAuction(user)
                .then((response) => {
                    //this.userId = response.data.tempUser;
                    this.getActiveTask();
                }).catch((error) => {

            });
        },

        getActiveTask(){
            setTimeout(
                api.getActiveTask(this.loggedUser)
                    .then((response) => {
                        this.activeTasks = response.data;
                        // if(this.activeTasks[0].name ==='Izbor kategorije'){
                        //     this.tempModel[0].value = "";
                        // }
                        this.getFields(this.activeTasks[0]);
                    }).catch((error) => {

                }),2000)
        },

        getFields(task) {

            if(!task || task===null){
                this.selectedTask ={
                    name:"",
                    id: ""
                }
            }else{
                this.selectedTask = task;
            }
            api.getFields(task.id)
                .then((response) => {
                    this.tempModel = response.data;
                }).catch((error) => {

            });
        },
        submit() {
            // if(this.addedCategory === '' && this.activeTasks[0].name ==='Izbor kategorije') {
            //     alert("Lista kategorija ne sme biti prazna");
            //     return;
            // }
            api.submitForm(this.selectedTask.id,this.tempModel)
                .then((response) => {
                    this.getActiveTask();
                }).catch((error) => {

            });
        },
        addCategory(){
            if(this.addedCategory.indexOf(this.selectedCategory)===-1){
                this.addedCategory += this.selectedCategory + ";";
            }
            // if(this.activeTasks[0].name ==='Izbor kategorije'){
            //     this.tempModel[0].value = this.addedCategory;
            // }
        },
        getCategory() {
            api.getCategory()
                .then((response) => {
                    this.listOfCategory = response.data;
                }).catch((error) => {

            });
        },
        isLogged(){
            if(localStorage.getItem('username')===null || localStorage.getItem('username')===''){
                this.$router.push('/login');
            }else{
                this.loggedUser = localStorage.getItem('username');
                this.startAuction(this.loggedUser);

            }
        }
    },
    components: {},
    beforeMount() {
        this.isLogged();

    }
}
