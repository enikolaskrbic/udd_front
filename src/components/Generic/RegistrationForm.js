/* import libs */
import Vue from 'vue';
import restApi from '../../http-common'
import swal from 'sweetalert2'
import VueRouter from 'vue-router'
import Api from '../../services/networkManager'
/* import api */

/* init */
const router = new VueRouter();
let api = new Api();

export default {
    name: 'register',
    data() {
        return {
            tempModel: [
                {
                    type: null,
                    name: null,
                    label: null,
                    value: null,
                    required: true,
                    writable: true,
                    readable: true,
                    values: [
                        {
                            name: "",
                            value: ""
                        }
                    ]
                }
            ],
            userId : "",
            activeTasks : [
                {
                    id: "",
                    name : ""
                }

            ],
            selectedCategory : "",
            addedCategory:"",
            listOfCategory : [
                {
                    id: "",
                    ime: "",
                    activitiId: "",
                    opis: "",
                    korisnickoIme: ""
                }
            ],
            selectedTask: {
                id: "",
                name : ""
            }
        }
    },
    methods: {
        startRegister(){
            api.startRegistration()
                .then((response) => {
                    this.userId = response.data.tempUser;
                    this.getActiveTask();
                }).catch((error) => {

            });
        },

        getActiveTask(){
            setTimeout(
            api.getActiveTask(this.userId)
                .then((response) => {
                    this.activeTasks = response.data;
                    if(this.selectedTask.name ==='Choose category and input max distance'){
                        this.tempModel[0].value = "";
                    }
                    this.getFields(this.activeTasks[0]);
                }).catch((error) => {

            }),2000)
        },

        getFields(task) {
            this.selectedTask = task;
            api.getFields(task.id)
                .then((response) => {
                    this.tempModel = response.data;

                }).catch((error) => {

            });
        },
        submit() {
            if(this.addedCategory === '' && this.selectedTask.name ==='Choose category and input max distance') {
                alert("Lista kategorija ne sme biti prazna");
                return;
            }
            api.submitForm(this.selectedTask.id,this.tempModel)
                .then((response) => {
                    this.getActiveTask();
                }).catch((error) => {

            });
        },
        addCategory(){
            if(this.addedCategory.indexOf(this.selectedCategory)===-1){
                this.addedCategory += this.selectedCategory + ";";
            }
            if(this.selectedTask.name ==='Choose category and input max distance'){
                this.tempModel[0].value = this.addedCategory;
            }
        },
        getCategory() {
            api.getCategory()
                .then((response) => {
                    this.listOfCategory = response.data;
                }).catch((error) => {

            });
        }
    },
    components: {},
    beforeMount() {
        this.startRegister();
        this.getCategory();
    }
}
