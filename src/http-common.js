import axios from 'axios';
let restApi = axios.create({
    // baseURL: 'http://localhost:8080/api/',
    baseURL: 'http://192.168.1.5:8080/api/',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    }
});

restApi.interceptors.request.use(function (config) {
        return config;
    }, function (error) {
        return Promise.reject(error);
    });

restApi.interceptors.response.use(function(response) {
        return response;
    }, function (error) {
        return this.$sessionService.handleErrorResponse(error);
    });

export default restApi

