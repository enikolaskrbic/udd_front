import restApi from '../http-common';

export default class NetworkManager {
    signIn(model) {
        return restApi({
            method: 'post',
            url: '/login',
            data:{
                korisnickoIme: model.email,
                lozinka: model.password
            }
        });
    }


    getFields(taskId) {
        return restApi({
            method: 'get',
            url: '/tasks/form/'+taskId,
        });
    }

    startRegistration() {
        return restApi({
            method: 'post',
            url: '/start-register',
        });
    }

    getActiveTask(userId) {
        return restApi({
            method: 'get',
            url: '/tasks/'+userId,
        });
    }

    submitForm(taskId,model) {
        return restApi({
            method: 'post',
            url: '/tasks/form/'+taskId,
            data : model
        });
    }

    getCategory() {
        return restApi({
            method: 'get',
            url: '/category'
        });
    }

    startAuction(korisnickoIme){
        return restApi({
            method: 'post',
            url: '/aukcija',
            data: {
                korisnik : korisnickoIme
            }
        });
    }
}
