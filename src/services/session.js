export default {

    /*
        * route change watcher
        * @param {object} router - router defined in main.js
    */
    init(router) {
    },

    /*
        * returns access token from local storage
    */
    getAccessToken() {
        return localStorage.getItem('ACCESS_TOKEN');
    },

    /*
        * redirects to login page if access token doesn't exist in local storage
    */
    isSessionActive() {
        if(this.accessToken() === null) {
            window.location = '/#/login';
        } else {
            return true;
        }
    },

    /*
        * saves user object props to local storage
        * @param user {object}
    */
    saveUser(user) {
        localStorage.setItem('ACCESS_TOKEN', 'Bearer ' + user.accessToken);
        localStorage.setItem('USER_FIRSTNAME', user.firstName);
    },

    /*
        * deletes user object props from local storage
    */
    destroyUser() {
        localStorage.removeItem('ACCESS_TOKEN');
        localStorage.removeItem('USER_FIRSTNAME');
    },

    /*
        * returns value for given field from local storage
        * @param field - {string}
    */
    get(field) {
        return localStorage.getItem(field);
    },

    /*
        * sets value for given field name to local storage
        * @param field {string} 
        * @param data {any}
    */
    set(field, data) {
        localStorage.setItem(field, data);
    },

    handleErrorResponse(error) {
        let currentPathName = router.history.current.path;

        if (401 === error.response.status && currentPathName !== '/') {
            swal('Error', 'Session not valid', 'error');
            this.logout();
        } else {
            return Promise.reject(error);
        }
    }
}